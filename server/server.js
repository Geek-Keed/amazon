const express = require('express');
const morgan  = require('morgan');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

mongoose.connect(process.env.DATABASE, { useNewUrlParser: true, useUnifiedTopology: true },(err)=>{
	if(err){
		console.log(err)
	}else{
		console.log('Connected to the database');
	}
})
//Models
const User = require('./models/user');

//Middleware
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//Apis
const productRoutes = require('./routes/product');
const CategoryRoutes = require('./routes/category');
const ownerRoutes = require('./routes/owner');

app.use("/api",productRoutes);
app.use("/api",CategoryRoutes);
app.use("/api",ownerRoutes);

app.listen(3000,err=>{
	if(err){
		console.log(err);
	}else{
		console.log('Listening on PORT',3000);
	}
})

