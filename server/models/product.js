const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
	category:{type:Schema.Types.ObjectId,ref:'Category'},//one-to-one relationship with CategorySchema
	owner:{type:Schema.Types.ObjectId,ref:'Owner'},//one-to-one relationship with OwnerSchema
	title:String,
	description:String,
	photo: {type: String},
	price:Number,
	stockQuantity:Number,
    rating:[Number]
});

module.exports = mongoose.model('Product',ProductSchema);