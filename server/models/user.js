const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	name:String,
	email:{type:String,unique:true,required:true},
	password:{type:String,required:true},
	address:{type:Schema.Types.ObjectId,ref:'Address'}//one-to-one relationship with UserSchema to AddressSchema
});

module.exports = mongoose.model('User',UserSchema);