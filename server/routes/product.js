const express = require('express');
const router = require('express').Router();
const Product = require('../models/product');
const upload = require('../middlewares/photo-upload');


//POST request - create a new product

router.post('/products',upload.single('photo'),async (req,res)=>{
	try{
        
		let product = new Product();
		product.title = req.body.title;
		product.description = req.body.description;
		product.photo = req.file.toString();
		product.price = req.body.price;
		product.stockQuantity = req.body.stockQuantity;
		product.rating = req.body.rating;
		product.category = req.body.category;
		product.owner = req.body.owner;

		await product.save();

		res.json({
			status:true,
			message:'Successfully saved'
		});
	} catch(err){
		res.status(500).json({
			success:false,
			message:err.message
		});
	}
})

//GET request - get all products
router.get('/products',async (req,res)=>{
	try{
		let products = await Product.find();
		res.json({
			success:true,
			products:products
		});
	}catch(err){
		res.status(500).json({
			success:false,
			message:err.message
		});
	}
});

//GET request - get a single product
router.get('/products/:id',async (req,res)=>{
	try{
		let product = await Product.findOne({_id:req.params.id});
		res.json({
			success:true,
			product:product
		});
	}catch(err){
		res.status(500).json({
			success:false,
			message:err.message
		});
	}
});

//PUT request - Update a single product
router.put('/products/:id',upload.single('photo'),async (req,res)=>{
	try{
		let product = await Product.findOneAndUpdate(
			{_id:req.params.id},
			{
			$set:{
				title:req.body.title,
				price:req.body.price,
				category:req.body.categoryID,
				photo:req.file.toString(),
				description:req.body.description,
				owner:req.body.ownerID,
				rating:req.body.rating,
				stockQuantity:req.body.stockQuantity
			   }
		   },
		   {upsert:true});
		res.json({
			success:true,
			updatedProduct:product
		});
	}catch(err){
		res.status(500).json({
			success:false,
			message:err.message
		});
	}
});
//DELETE request - delete a single product
router.delete('/products/:id',async (req,res)=>{
	try{
		let deleteProduct = await Product.findOneAndDelete({_id:req.params.id});
		
		if(deleteProduct){
		  res.json({
			success:true,
			message:"successfully deleted product"
		 });
	   }
	}catch(err){
		res.status(500).json({
			success:false,
			message:err.message
		});
	}
});


module.exports = router;